# Catchme Scripts

## catchme block
![Catchme block](./ssscroll.gif "catchmeblock")

Each time the script is called, the text is scrolled, for using with your statusbar.

- Make sure the folder ~/.cache/ exists.
- Download and place the script on your PATH.
- This script uses `ssscroll` by default, but you can use any other scroll utility.
